#ifndef TITANIUM_ETC_HPP_
#define TITANIUM_ETC_HPP_

namespace Titanium::etc {

#define PACKED __attribute__((packed))

  enum class eCommonType_t {
    B01, // bool (int8)
    U08, // uint8
    S08, // int8
    U16, // uint16
    S16, // int16
    U32, // uint32
    S32, // int32
    F32, // float
    U64, // uint64
    S64, // int64
    F64  // double
  };

}

#endif /* TITANIUM_ETC_HPP_ */
