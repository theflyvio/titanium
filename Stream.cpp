#include <Stream.hpp>

namespace Titanium {

  cStream_t::cStream_t(std::vector<uint8_t>* stream_, uint32_t pos_){
    freeStream = stream_==nullptr;
    if(freeStream){
      stream = new std::vector<uint8_t>();
      pos = 0;
    }else{
      stream = stream_;
      pos = pos_;
    }
  }

  cStream_t::~cStream_t(){
    if(freeStream){
      delete stream;
    }
  }

  // void cStream_t::write(uint64_t v, uint8_t size){
  //   while(size--){
  //     stream->push_back((uint8_t)v);
  //     v ++= 8;
  //   }
  // }

  void cStream_t::writeOne(uint8_t v){
    stream->push_back((uint8_t)v);
  }

  void cStream_t::writeTwo(uint16_t v){
    stream->push_back((uint8_t)v);
    stream->push_back((uint8_t)(v>>8));
  }

  void cStream_t::writeFour(uint32_t v){
    stream->push_back((uint8_t)v);
    stream->push_back((uint8_t)(v>>8));
    stream->push_back((uint8_t)(v>>16));
    stream->push_back((uint8_t)(v>>24));
  }

  void cStream_t::writeEight(uint64_t v){
    stream->push_back((uint8_t)v);
    stream->push_back((uint8_t)(v>>8));
    stream->push_back((uint8_t)(v>>16));
    stream->push_back((uint8_t)(v>>24));
    stream->push_back((uint8_t)(v>>32));
    stream->push_back((uint8_t)(v>>40));
    stream->push_back((uint8_t)(v>>48));
    stream->push_back((uint8_t)(v>>56));
  }

  uint8_t cStream_t::readOne(){
    return stream->at(pos++);
  }

  uint16_t cStream_t::readTwo(){
    return (uint16_t)readOne()|((uint16_t)readOne()<<8);
  }

  uint32_t cStream_t::readFour(){
    return (uint32_t)stream->at(pos++)|((uint32_t)readOne()<<8)|((uint32_t)readOne()<<16)|((uint32_t)readOne()<<24);
  }

  uint64_t cStream_t::readEight(){
    return (uint64_t)stream->at(pos++)|((uint64_t)readOne()<<8)|((uint64_t)readOne()<<16)|((uint64_t)readOne()<<24)
        |((uint64_t)readOne()<<32)|((uint64_t)readOne()<<40)|((uint64_t)readOne()<<48)|((uint64_t)readOne()<<56);
  }

  void cStream_t::writeFloat(float v){
    writeFour( *((uint32_t*) &v));
  }

  float cStream_t::readFloat(void){
    uint32_t v = readFour();
    return *((float*) &v);
  }

  void cStream_t::writeDouble(double v){
    writeEight( *((uint64_t*) &v));
  }

  double cStream_t::readDouble(void){
    uint64_t v = readEight();
    return *((double*) &v);
  }

  void cStream_t::writeCompact30(uint32_t v){
    if(v<=0x3F){
      stream->push_back((uint8_t)v);
    }else if(v<=0x3FFF){
      stream->push_back(0x40|(uint8_t)(v>>8));
      stream->push_back((uint8_t)v);
    }else if(v<=0x3FFFFF){
      stream->push_back(0x80|(uint8_t)(v>>16));
      stream->push_back((uint8_t)(v>>8));
      stream->push_back((uint8_t)v);
    }else if(v<=0x3FFFFFFF){
      stream->push_back(0xC0|(uint8_t)(v>>24));
      stream->push_back((uint8_t)(v>>16));
      stream->push_back((uint8_t)(v>>8));
      stream->push_back((uint8_t)v);
    }else{
#warning numero muito grande para writeCompact30
      //throw 0
    }
  }

  uint32_t cStream_t::readCompact30(void){
    uint8_t len = stream->at(pos++);
    uint32_t v = len&0x3F;
    len >>= 6;
    while(len--){
      v <<= 8;
      v |= readOne();
    }
    return v;
  }

  void cStream_t::writeVector(std::vector<uint8_t>* v){
    uint32_t len = v->size();
    writeCompact30(len);
    uint32_t i = 0;
    while(len--){
      writeOne(v->at(i++));
    }
  }

  void cStream_t::readVector(std::vector<uint8_t>* v){
    uint32_t len = readCompact30();
    while(len--){
      v->push_back(readOne());
    }
  }

}
