/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : TitaniumFollower.cpp
 * @brief         : Program body.
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#include <TitaniumFollower.hpp>

namespace Titanium {
  cTitaniumFollower::cTitaniumFollower(void)
      : cTitanium(false){
  }

  void cTitaniumFollower::createSystem(void){
    /* Specific follower initialization */
#warning appOrder esta sendo definido com um valor fixo
    setAppKey(1);
  }
}
