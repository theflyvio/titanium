#ifndef COMMON_TSTREAM_HPP_
#define COMMON_TSTREAM_HPP_

#include <stdint.h>
#include <vector>

namespace Titanium {

  class cStream_t {
    private:
      bool freeStream;
      std::vector<uint8_t>* stream;
      uint16_t pos;

      void writeOne(uint8_t v);
      void writeTwo(uint16_t v);
      void writeFour(uint32_t v);
      void writeEight(uint64_t v);
      uint8_t readOne();
      uint16_t readTwo();
      uint32_t readFour();
      uint64_t readEight();

    public:
      cStream_t(std::vector<uint8_t>* stream_, uint32_t pos_);
      ~cStream_t();

      inline void writeU08(uint8_t v){
        writeOne(v);
      }
      inline uint8_t readU08(void){
        return readOne();
      }

      inline void writeS08(int8_t v){
        writeOne((uint8_t)v);
      }
      inline int8_t readS08(void){
        return (int8_t)readOne();
      }

      inline void writeU16(uint16_t v){
        writeTwo(v);
      }
      inline uint16_t readU16(void){
        return readTwo();
      }

      inline void writeS16(int16_t v){
        writeTwo((uint16_t)v);
      }
      inline int16_t readS16(void){
        return (int16_t)readTwo();
      }

      inline void writeU32(uint32_t v){
        writeFour(v);
      }
      inline uint32_t readU32(void){
        return readFour();
      }

      inline void writeS32(int32_t v){
        writeFour((uint32_t)v);
      }
      inline int32_t readS32(void){
        return (int32_t)readFour();
      }

      inline void writeU64(uint64_t v){
        writeEight(v);
      }
      inline uint64_t readU64(void){
        return readEight();
      }

      inline void writeS64(int64_t v){
        writeFour((uint64_t)v);
      }
      inline int64_t readS64(void){
        return (int64_t)readFour();
      }

      void writeFloat(float v);
      float readFloat(void);

      void writeDouble(double v);
      double readDouble(void);

      void writeCompact30(uint32_t v);
      uint32_t readCompact30(void);

      void writeVector(std::vector<uint8_t>* v);
      void readVector(std::vector<uint8_t>* v);

  };

}

#endif /* COMMON_TSTREAM_HPP_ */
