#ifndef TITANIUM_DB_SFF_USR_TUSERLIST_HPP_
#define TITANIUM_DB_SFF_USR_TUSERLIST_HPP_

#include <Usr/User.hpp>
#include <vector>

namespace Titanium::db {

  class cUserList_t {
    private:
      std::vector<cUser_t*> userList;

    public:
      cUserList_t(void) {}
      ~cUserList_t() {}
      uint8_t getUserCount(void){ return userList.size();}
      void addUser(char *name){} // criacao interna? quantidade pré-determinada de usuarios (pode ser "nao-ponteiros")
      //void addUser(cUser_t *user); // criacao externa
      cUser_t *getUserByKey(uint16_t key);
      cUser_t *getUserByIndex(uint8_t index){ return userList[index]; }
      
  };

}

#endif /* TITANIUM_DB_SFF_USR_TUSERLIST_HPP_ */
