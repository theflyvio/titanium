#ifndef TITANIUM_DB_USR_REGISTERUSERNAME_HPP_
#define TITANIUM_DB_USR_REGISTERUSERNAME_HPP_

#include <Register.hpp>

namespace Titanium::db {

  class cRegisterUsername_t : public cRegister, public cValueStringImpl_t<20> {
    private:
      inline cUserProfile_t getProfileRd(void) const override { return cUserProfile_t(eUserProfile_t::FULL); }
      cUserProfile_t getProfileWr(void) const override { return cUserProfile_t(eUserProfile_t::FULL); }

    protected:
      cRegisterUsername_t(char const * ini_, eRegisterOrigin_t origin_);

      uint8_t onGetU08(uint16_t index_) const override {
        return this->getValue(index_);
      }

      bool onSetU08(uint8_t value_, uint16_t index_) override {
        bool const changed = value_!=this->getValue(index_);
        this->setValue(value_, index_);
        return changed;
      }

    public:
      char get(uint16_t index_) const override { return getU08Sys(index_); }
      void set(char value_, uint16_t index_) override { setU08Sys(value_, index_); }

      friend cRegisterUsername_t *cRegLocalCfgUsername(char const *ini_);
      friend cRegisterUsername_t *cRegRemoteCfgUsername(char const *ini_);
  };

  inline cRegisterUsername_t *cRegLocalCfgUsername(char const *ini_){
    return new cRegisterUsername_t(ini_,eRegisterOrigin_t::LOCAL);
  }

  inline cRegisterUsername_t *cRegRemoteCfgUsername(char const *ini_){
    return new cRegisterUsername_t(ini_,eRegisterOrigin_t::REMOTE);
  }

}

#endif /* TITANIUM_DB_USR_REGISTERUSERNAME_HPP_ */
