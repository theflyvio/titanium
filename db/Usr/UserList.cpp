#include <Usr/UserList.hpp>

namespace Titanium::db {

  cUser_t *cUserList_t::getUserByKey(uint16_t key){
    cUser_t *user = nullptr;
    uint16_t i=0;
    while(i<getUserCount() && user==nullptr){
      if(userList[i]->getKey()==key){
        user = userList[i];
      }else{
        i++;
      }
    }
    return user;
  }

}
