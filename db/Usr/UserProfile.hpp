#ifndef TITANIUM_DB_SFF_USR_TUSERPROFILE_HPP_
#define TITANIUM_DB_SFF_USR_TUSERPROFILE_HPP_

#include <stdint.h>

#include "etc.hpp"

namespace Titanium::db {

  enum class PACKED eUserProfile_t : uint8_t {
    EMPTY = 0x00,
    NON = 0x01,
    BAS = 0x02,
    ADV = 0x04,
    ENG = 0x08,
    AUD = 0x10,
    MTN = 0x20,
    //FCT = 0x40,
    FULL = 0x3F
  };

  class PACKED cUserProfile_t {
    private:
      uint8_t key;

    public:
      cUserProfile_t(eUserProfile_t p=eUserProfile_t::EMPTY) { key = (uint8_t)p; }
      inline void set(eUserProfile_t p) { key = (uint8_t)p; }
      inline void set(cUserProfile_t p) { key = p.key; }
      inline void add(eUserProfile_t p) { key |= (uint8_t)p; }
      inline void add(cUserProfile_t p) { key |= p.key; }
      inline void remove(eUserProfile_t p) { key &= ~(uint8_t)p; }
      inline void remove(cUserProfile_t p) { key &= ~p.key; }
      inline bool allow(eUserProfile_t p) { return (key&(uint8_t)p)>0; }
      inline bool allow(cUserProfile_t p) { return (key&p.key)>0; }

      inline cUserProfile_t operator|(eUserProfile_t b){
        return (eUserProfile_t)(key|(uint8_t)b);
      }
  };

  inline cUserProfile_t operator|(eUserProfile_t a, eUserProfile_t b){
    return (eUserProfile_t)((uint8_t)a|(uint8_t)b);
  }


}

#endif /* TITANIUM_DB_SFF_USR_TUSERPROFILE_HPP_ */
