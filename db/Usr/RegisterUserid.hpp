#ifndef TITANIUM_DB_USR_REGISTERID_HPP_
#define TITANIUM_DB_USR_REGISTERID_HPP_

#include <RegisterU08Array.hpp>

namespace Titanium::db {

  class cRegisterUserid_t : public cRegisterU08Array_t<4> {
    private:
      inline cUserProfile_t getProfileRd(void) const override { return cUserProfile_t(eUserProfile_t::FULL); }
      cUserProfile_t getProfileWr(void) const override { return cUserProfile_t(eUserProfile_t::FULL); }

    protected:
      cRegisterUserid_t(uint32_t ini_, eRegisterOrigin_t origin_);

    public:
      uint32_t getId(void);

      friend cRegisterUserid_t *cRegLocalCfgUserid(uint32_t ini_);
      friend cRegisterUserid_t *cRegRemoteCfgUserid(void);
  };

  inline cRegisterUserid_t *cRegLocalCfgUserid(uint32_t ini_){
    return new cRegisterUserid_t(ini_, eRegisterOrigin_t::LOCAL);
  }

  inline cRegisterUserid_t *cRegRemoteCfgUserid(void){
    return new cRegisterUserid_t(0, eRegisterOrigin_t::REMOTE);
  }

}

#endif /* TITANIUM_DB_USR_REGISTERID_HPP_ */
