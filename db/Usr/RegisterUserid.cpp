#include <RegisterUserid.hpp>

namespace Titanium::db {

  cRegisterUserid_t::cRegisterUserid_t(uint32_t ini_, eRegisterOrigin_t origin_)
                    :cRegisterU08Array_t((uint8_t *)&ini_,
                                          getSizeofValue(), // iniLength_
                                          0, // instance_
                                          true, // cfg_
                                          0x1111, // description_,
                                          origin_, 
                                          eRegisterPersistence_t::ON_CHANGE){
    #warning DESCRIPTION NAO DEFINIDA
  }

  uint32_t cRegisterUserid_t::getId(void){
    uint32_t v;
    getValueBulk((uint8_t *)&v, getSizeofValue());
    return v;
  }

}
