#include <Register.hpp>
#include <Usr/User.hpp>

namespace Titanium::db {

  bool cUser_t::allowRead(cRegister &r){
    return r.allowRead(this);
  }

  bool cUser_t::allowWrite(cRegister &r){
    return r.allowWrite(this);
  }

  bool cUser_t::allowRead(const std::vector<cRegister *> &v){
    uint16_t i = 0;
    bool ok = true;
    while(ok && i < v.size()) {
      ok = allowRead(*v[i]);
      i++;
    }
    return ok;
 }

  bool cUser_t::allowWrite(const std::vector<cRegister *> &v){
    uint16_t i = 0;
    bool ok = true;
    while(ok && i < v.size()) {
      ok = allowWrite(*v[i]);
      i++;
    }
    return ok;
 }

}
