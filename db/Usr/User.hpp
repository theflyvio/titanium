#ifndef TITANIUM_DB_SFF_USR_TUSER_HPP_
#define TITANIUM_DB_SFF_USR_TUSER_HPP_

#include <stdint.h>
#include <Usr/UserProfile.hpp>
#include <vector>

namespace Titanium::db {

  class cRegister;

  class cUser_t {
    private:
      uint16_t key;
      uint32_t tagId;
      uint8_t username[25];
      cUserProfile_t profile;
      uint32_t passwordHash;

    public:
      inline uint16_t getKey(void){ return key; }
      inline cUserProfile_t getProfile(void){ return profile; }
      inline void setProfile(cUserProfile_t p) { profile = p; }

      bool allowRead(cRegister &r);
      bool allowWrite(cRegister &r);
      bool allowRead(const std::vector<cRegister *> &v);
      bool allowWrite(const std::vector<cRegister *> &v);
  };

}

#endif /* TITANIUM_DB_SFF_USR_TUSER_HPP_ */
