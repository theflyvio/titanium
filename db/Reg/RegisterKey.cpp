#include <RegisterKey.hpp>

namespace Titanium::db {

  cRegisterKey_t::cRegisterKey_t(uint8_t instance,
                                 bool cfg,
                                 uint16_t description){
    composition.app         = (uint8_t)eAppKey::eUnknown;
    composition.instance    = instance;
    composition.cfg         = cfg;
    composition.description = description;
  }

  void cRegisterKey_t::setApp(eAppKey app_){
    if(composition.app!=(uint8_t)eAppKey::eUnknown){
      throw 0;
    }
    composition.app = app_.getValue();
  }

}
