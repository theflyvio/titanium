#ifndef TITANIUM_DB_SFF_REG_TREGISTERFORMAT_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERFORMAT_HPP_

#include <stdint.h>
#include <etc.hpp>
#include <RegisterOrigin.hpp>
#include <RegisterPersistence.hpp>

namespace Titanium::db {

  using namespace Titanium::etc;

  class PACKED cRegisterFormat_t final {
    private:
      struct PACKED {
        eRegisterOrigin_t      origin      : 1;
        eRegisterPersistence_t persistence : 2;
        eCommonType_t          ctype       : 5;
      } format;

    public:
      void setOrigin(eRegisterOrigin_t origin);
      eRegisterOrigin_t getOrigin(void) const;
      void setPersistence(eRegisterPersistence_t persistence);
      eRegisterPersistence_t getPersistence(void) const;
      void setCType(eCommonType_t ctype);
      eCommonType_t getCType(void) const;

  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERFORMAT_HPP_ */
