#ifndef TITANIUM_DB_SFF_REG_TREGISTERONE_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERONE_HPP_

#include <stdint.h>
#include <etc.hpp>
#include <Register.hpp>

namespace Titanium::db {

  class PACKED cRegisterOne_t : public cRegister {
    private:
      uint8_t value;
      cUserProfile_t rd;
      cUserProfile_t wr;
      inline cUserProfile_t getProfileRd(void) const override { return rd; }
      inline cUserProfile_t getProfileWr(void) const override { return wr; }

    protected:
      cRegisterOne_t(uint8_t ini_,
                    uint8_t instance_,
                    bool cfg_,
                    uint16_t description_,
                    eRegisterOrigin_t origin_,
                    eRegisterPersistence_t persistence_);

      uint8_t onGetU08(uint16_t index_) const override;
      bool onSetU08(uint8_t value_, uint16_t index_) override;

      inline void configure(cUserProfile_t rd_, cUserProfile_t wr_){
        rd = rd_;
        wr = wr_;
      }

    public:
      using cRegister::getU08Sys;
      using cRegister::setU08Sys;
      //inline uint8_t get(void) const override { return getU08Sys(0); }
      //inline void set(uint8_t value_) override { setU08Sys(value_, 0); }

      friend cRegisterOne_t *cRegLocalPrcOne(uint8_t instance_,
                                                           uint16_t description_,
                                                           eRegisterPersistence_t persistence_,
                                                           uint8_t ini_,
                                                           cUserProfile_t rd_,
                                                           cUserProfile_t wr_);
                                            
  };

  inline cRegisterOne_t *cRegLocalPrcOne(uint8_t instance_,
                                                       uint16_t description_,
                                                       eRegisterPersistence_t persistence_,
                                                       uint8_t ini_,
                                                       cUserProfile_t rd_,
                                                       cUserProfile_t wr_){
    cRegisterOne_t *r = new cRegisterOne_t(ini_,instance_,false,description_,eRegisterOrigin_t::LOCAL,persistence_);
    r->configure(rd_, wr_);
    return r;
  }

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERU08_HPP_ */
