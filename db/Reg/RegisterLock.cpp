#include <RegisterLock.hpp>
#include <Titanium.hpp>

namespace Titanium::db {

  cRegisterLock_t::cRegisterLock_t(cRegister *reg_){
    reg = reg_;
  }

  cRegisterLock_t::~cRegisterLock_t(){
    unlock();
  }

  bool cRegisterLock_t::lock(void){
    if(locked()){
      #warning register already locked (ou controle de profundidade de lock?)
      //throw 0
    }
    password = cTitanium::random(255)+1;
    bool ok =reg->setPassword(password);
    if(!ok){
      password = 0;
    }
    return ok;
  }

  void cRegisterLock_t::unlock(void){
    if(!locked()){
      #warning register already locked (ou controle de profundidade de lock?)
      //throw 0
    }
    if(!reg->clearPassword(password)){
      #warning could not unlock register!
      //throw 0
    }
    password = 0;
  }

}
