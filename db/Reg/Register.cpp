#include <Database.hpp>
#include <Register.hpp>

namespace Titanium::db {

  cRegister::cRegister(uint8_t instance_,
                           bool cfg_,
                           uint16_t description_,
                           eCommonType_t ctype_,
                           eRegisterOrigin_t origin_,
                           eRegisterPersistence_t persistence_)
              :password(0),
               key(instance_, cfg_, description_){
    format.setPersistence(persistence_);
    format.setOrigin(origin_);
    format.setCType(ctype_);
  }

  bool cRegister::setPassword(uint8_t password_){
    bool ok = !password;
    if(ok){
      password = password_;
    }
    return ok;
  }

  bool cRegister::clearPassword(uint8_t password_){
    bool ok = password_==password;
    if(ok){
      password = 0;
    }
    return ok;
  }

  void cRegister::checkPassword(uint8_t password_) const { 
    if(password_!=password) {
      throw 0;
    } 
  }

  void cRegister::requireUser(cUser_t *user_) const {
    if(user_==nullptr){
      throw 0;
    }
  }

  void cRegister::requireUserWr(cUser_t *user_) const {
    if(user_==nullptr || !allowWrite(user_)){
      throw 0;
    }
  }

  void cRegister::requireUserRd(cUser_t *user_) const {
    if(user_==nullptr || !allowRead(user_)){
      throw 0;
    }
  }

  void cRegister::onRegisterChanged(void){
    cDatabase::getDatabase()->registerChanged(this);
  }

  //

  uint8_t  cRegister::getU08User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetU08(index_);
  }

  void cRegister::setU08User(cUser_t *user_, uint8_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetU08(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setU08Sys(uint8_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetU08(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  int8_t  cRegister::getS08User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetS08(index_);
  }

  void cRegister::setS08User(cUser_t *user_, int8_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetS08(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setS08Sys(int8_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetS08(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  uint16_t  cRegister::getU16User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetU16(index_);
  }

  void cRegister::setU16User(cUser_t *user_, uint16_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetU16(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setU16Sys(uint16_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetU16(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  int16_t  cRegister::getS16User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetS16(index_);
  }

  void cRegister::setS16User(cUser_t *user_, int16_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetS16(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setS16Sys(int16_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetS16(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  uint32_t  cRegister::getU32User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetU32(index_);
  }

  void cRegister::setU32User(cUser_t *user_, uint32_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetU32(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setU32Sys(uint32_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetU32(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  int32_t  cRegister::getS32User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetS32(index_);
  }

  void cRegister::setS32User(cUser_t *user_, int32_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetS32(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setS32Sys(int32_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetS32(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  uint64_t  cRegister::getU64User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetU64(index_);
  }

  void cRegister::setU64User(cUser_t *user_, uint64_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetU64(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setU64Sys(uint64_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetU64(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  int64_t  cRegister::getS64User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetS64(index_);
  }

  void cRegister::setS64User(cUser_t *user_, int64_t value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetS64(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setS64Sys(int64_t value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetS64(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  float  cRegister::getF32User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetF32(index_);
  }

  void cRegister::setF32User(cUser_t *user_, float value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetF32(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setF32Sys(float value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetF32(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  double cRegister::getF64User(cUser_t *user_, uint16_t index_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetF64(index_);
  }

  void cRegister::setF64User(cUser_t *user_, double value_, uint16_t index_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetF64(value_, index_)){
      onRegisterChanged();
    }
  }

  void cRegister::setF64Sys(double value_, uint16_t index_, uint8_t password_){
    checkPassword(password_); 
    if(onSetF64(value_, index_)){
      onRegisterChanged();
    }
  }

  //

  char *cRegister::getStringUser(cUser_t *user_) const { 
    requireUser(user_); 
    requireUserRd(user_);
    return onGetString();
  }

  void cRegister::setStringUser(cUser_t *user_, char *value_, uint8_t password_){
    requireUser(user_);
    requireUserWr(user_);
    checkPassword(password_); 
    if(onSetString(value_)){
      onRegisterChanged();
    }
  }

  void cRegister::setStringSys(char *value_, uint8_t password_){
    checkPassword(password_); 
    if(onSetString(value_)){
      onRegisterChanged();
    }
  }

  
}
