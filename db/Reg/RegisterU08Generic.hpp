#ifndef TITANIUM_DB_REG_REGISTERU08GENERIC_HPP_
#define TITANIUM_DB_REG_REGISTERU08GENERIC_HPP_

#include <RegisterU08.hpp>

namespace Titanium::db {

  class cRegisterU08Generic_t : public cRegisterU08_t {
    private:
      cUserProfile_t rd;
      cUserProfile_t wr;
      inline cUserProfile_t getProfileRd(void) const override { return cUserProfile_t(eUserProfile_t::FULL); }
      inline cUserProfile_t getProfileWr(void) const override { return cUserProfile_t(eUserProfile_t::FULL); }
    
    protected:
      cRegisterU08Generic_t(uint8_t ini_,
                            uint8_t instance_,
                            bool cfg_,
                            uint16_t description_,
                            eRegisterOrigin_t origin_,
                            eRegisterPersistence_t persistence_);

      inline void configure(cUserProfile_t rd_, cUserProfile_t wr_){
        rd = rd_;
        wr = wr_;
      }

      friend cRegisterU08Generic_t *cRegLocalPrcU08Generic(uint8_t instance_,
                                                           uint16_t description_,
                                                           eRegisterPersistence_t persistence_,
                                                           uint8_t ini_,
                                                           cUserProfile_t rd_,
                                                           cUserProfile_t wr_);
                                            
      friend cRegisterU08Generic_t *cRegRemotePrcU08Generic(uint8_t instance_,
                                                            uint16_t description_,
                                                            eRegisterPersistence_t persistence_,
                                                            uint8_t ini_);

      friend cRegisterU08Generic_t *cRegLocalCfgU08Generic(uint8_t instance_,
                                                           uint16_t description_,
                                                           eRegisterPersistence_t persistence_,
                                                           uint8_t ini_,
                                                           cUserProfile_t rd_,
                                                           cUserProfile_t wr_);

      friend cRegisterU08Generic_t *cRegRemoteCfgU08Generic(uint8_t instance_,
                                                            uint16_t description_,
                                                            eRegisterPersistence_t persistence_,
                                                            uint8_t ini_);
  };

  inline cRegisterU08Generic_t *cRegLocalPrcU08Generic(uint8_t instance_,
                                                       uint16_t description_,
                                                       eRegisterPersistence_t persistence_,
                                                       uint8_t ini_,
                                                       cUserProfile_t rd_,
                                                       cUserProfile_t wr_){
    cRegisterU08Generic_t *r = new cRegisterU08Generic_t(ini_,instance_,false,description_,eRegisterOrigin_t::LOCAL,persistence_);
    r->configure(rd_, wr_);
    return r;
  }

  inline cRegisterU08Generic_t *cRegRemotePrcU08Generic(uint8_t instance_,
                                                        uint16_t description_,
                                                        eRegisterPersistence_t persistence_,
                                                        uint8_t ini_){
    return new cRegisterU08Generic_t(ini_,instance_,false,description_,eRegisterOrigin_t::REMOTE,persistence_);
  }

  inline cRegisterU08Generic_t *cRegLocalCfgU08Generic(uint8_t instance_,
                                                       uint16_t description_,
                                                       eRegisterPersistence_t persistence_,
                                                       uint8_t ini_,
                                                       cUserProfile_t rd_,
                                                       cUserProfile_t wr_){
    cRegisterU08Generic_t *r = new cRegisterU08Generic_t(ini_,instance_,true,description_,eRegisterOrigin_t::LOCAL,persistence_);
    r->configure(rd_, wr_);
    return r;
  }

  inline cRegisterU08Generic_t *cRegRemoteCfgU08Generic(uint8_t instance_,
                                                        uint16_t description_,
                                                        eRegisterPersistence_t persistence_,
                                                        uint8_t ini_){
    return new cRegisterU08Generic_t(ini_,instance_,true,description_,eRegisterOrigin_t::REMOTE,persistence_);
  }

}

#endif /* TITANIUM_DB_REG_REGISTERU08GENERIC_HPP_ */
