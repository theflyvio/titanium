#ifndef TITANIUM_DB_REG_REGISTER_HPP_
#define TITANIUM_DB_REG_REGISTER_HPP_

#include <stdint.h>
#include <string.h>
#include <User.hpp>
#include <UserProfile.hpp>
#include <etc.hpp>
#include <RegisterFormat.hpp>
#include <RegisterKey.hpp>

namespace Titanium::db {
  
  class cDatabase;

  /**
   * 
   * 
   * 
   */
  template <typename T>
  class cValueImpl_t {
    private:
      T value;

    protected:
      T getValue(void) const { return value; }
      void setValue(T value_){ value = value_; }
      
    public:
      uint16_t getSizeofElement(void) const { return sizeof(T); }
      uint16_t getSizeofValue(void) const { return sizeof(value); }
  };

  /**
   * 
   * 
   * 
   */
  template <typename T, uint16_t L>
  class cValueArrayImpl_t {
    private:
      T value[L];

      void checkIndex(uint16_t index_) const {
        if(index_>=L){
          throw 0;
        }
      }

    protected:
      T *getValue(void){
        return &value[0];
      }

      T getValue(uint16_t index_) const { 
        checkIndex(index_);
        return value[index_]; 
      }

      void setValue(T value_, uint16_t index_){ 
        checkIndex(index_);
        value[index_] = value_; 
      }

      void getValueBulk(T *start_, uint16_t elements_) const {
        if(elements_>0){
          checkIndex(elements_-1);
          memcpy(start_, value, elements_*getSizeofElement());
        }
      }

      void setValueBulk(T *start_, uint16_t elements_){ 
        if(elements_>0){
          checkIndex(elements_-1);
          uint16_t len = elements_*getSizeofElement();
          memcpy(value, start_, len);
        }
      }

    public:
#warning aumenta o tamanho de cada registro por causa da vtable
      virtual T get(uint16_t index_) const =0;
      virtual void set(T value_, uint16_t index_)=0;
      
      uint16_t getSizeofElement(void) const { return sizeof(T); }
      uint16_t getSizeofValue(void) const { return sizeof(value); }
  };

  /**
   * 
   * 
   * 
   */
  template <uint16_t L>
  class cValueStringImpl_t : public cValueArrayImpl_t<char, L> {
    public:
      char *getString(void){
        return this->getValue(); // perigoso, trabalhar somente com cópia?
      }

      void setString(char const *value_){
        uint16_t count = strlen(value_);
        for(uint16_t i=0; i<count; i++){
          this->set(value_[i], i);
        }
        this->set(0, count);
      }

  };

  /**
   * 
   * 
   * 
   */
  class PACKED cRegister {
    private:
      uint8_t password;
      cRegisterKey_t key;
      cRegisterFormat_t format;

      void checkPassword(uint8_t password_) const;
      void requireUser(cUser_t *user_) const;
      void requireUserWr(cUser_t *user_) const;
      void requireUserRd(cUser_t *user_) const;
      void onRegisterChanged(void);

      virtual cUserProfile_t getProfileRd(void) const =0; // { return cUserProfile_t(eUserProfile_t::FULL); }
      virtual cUserProfile_t getProfileWr(void) const =0; // { return cUserProfile_t(eUserProfile_t::FULL); }

    protected:
      cRegister(uint8_t instance_,
                  bool cfg_,
                  uint16_t description_,
                  eCommonType_t ctype_, 
                  eRegisterOrigin_t origin_, 
                  eRegisterPersistence_t persistence_);

      #define VIRTUAL_INVALID { throw 0; }
      inline virtual uint8_t  onGetU08(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual int8_t   onGetS08(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual uint16_t onGetU16(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual int16_t  onGetS16(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual uint32_t onGetU32(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual int32_t  onGetS32(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual uint64_t onGetU64(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual int64_t  onGetS64(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual float    onGetF32(uint16_t index_) const VIRTUAL_INVALID;
      inline virtual double   onGetF64(uint16_t index_) const VIRTUAL_INVALID;

      inline virtual bool onSetU08(uint8_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetS08(int8_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetU16(uint16_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetS16(int16_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetU32(uint32_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetS32(int32_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetU64(uint64_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetS64(int64_t value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetF32(float value_, uint16_t index_) VIRTUAL_INVALID;
      inline virtual bool onSetF64(double value_, uint16_t index_) VIRTUAL_INVALID;
    
      inline virtual char *onGetString(void) const VIRTUAL_INVALID;
      inline virtual bool onSetString(char *value_) VIRTUAL_INVALID;

      uint8_t  getU08User(cUser_t *user_, uint16_t index_) const;
      int8_t   getS08User(cUser_t *user_, uint16_t index_) const;
      uint16_t getU16User(cUser_t *user_, uint16_t index_) const;
      int16_t  getS16User(cUser_t *user_, uint16_t index_) const;
      uint32_t getU32User(cUser_t *user_, uint16_t index_) const;
      int32_t  getS32User(cUser_t *user_, uint16_t index_) const;
      uint64_t getU64User(cUser_t *user_, uint16_t index_) const;
      int64_t  getS64User(cUser_t *user_, uint16_t index_) const;
      float    getF32User(cUser_t *user_, uint16_t index_) const;
      double   getF64User(cUser_t *user_, uint16_t index_) const;

      inline uint8_t  getU08Sys(uint16_t index_) const { return onGetU08(index_); }
      inline int8_t   getS08Sys(uint16_t index_) const { return onGetS08(index_); }
      inline uint16_t getU16Sys(uint16_t index_) const { return onGetU16(index_); }
      inline int16_t  getS16Sys(uint16_t index_) const { return onGetS16(index_); }
      inline uint32_t getU32Sys(uint16_t index_) const { return onGetU32(index_); }
      inline int32_t  getS32Sys(uint16_t index_) const { return onGetS32(index_); }
      inline uint64_t getU64Sys(uint16_t index_) const { return onGetU64(index_); }
      inline int64_t  getS64Sys(uint16_t index_) const { return onGetS64(index_); }
      inline float    getF32Sys(uint16_t index_) const { return onGetF32(index_); }
      inline double   getF64Sys(uint16_t index_) const { return onGetF64(index_); }

      void setU08User(cUser_t *user_, uint8_t value_,  uint16_t index_, uint8_t password_=0);
      void setS08User(cUser_t *user_, int8_t value_,   uint16_t index_, uint8_t password_=0);
      void setU16User(cUser_t *user_, uint16_t value_, uint16_t index_, uint8_t password_=0);
      void setS16User(cUser_t *user_, int16_t value_,  uint16_t index_, uint8_t password_=0);
      void setU32User(cUser_t *user_, uint32_t value_, uint16_t index_, uint8_t password_=0);
      void setS32User(cUser_t *user_, int32_t value_,  uint16_t index_, uint8_t password_=0);
      void setU64User(cUser_t *user_, uint64_t value_, uint16_t index_, uint8_t password_=0);
      void setS64User(cUser_t *user_, int64_t value_,  uint16_t index_, uint8_t password_=0);
      void setF32User(cUser_t *user_, float value_,    uint16_t index_, uint8_t password_=0);
      void setF64User(cUser_t *user_, double value_,   uint16_t index_, uint8_t password_=0);

      void setU08Sys(uint8_t value_,  uint16_t index_, uint8_t password_=0);
      void setS08Sys(int8_t value_,   uint16_t index_, uint8_t password_=0);
      void setU16Sys(uint16_t value_, uint16_t index_, uint8_t password_=0);
      void setS16Sys(int16_t value_,  uint16_t index_, uint8_t password_=0);
      void setU32Sys(uint32_t value_, uint16_t index_, uint8_t password_=0);
      void setS32Sys(int32_t value_,  uint16_t index_, uint8_t password_=0);
      void setU64Sys(uint64_t value_, uint16_t index_, uint8_t password_=0);
      void setS64Sys(int64_t value_,  uint16_t index_, uint8_t password_=0);
      void setF32Sys(float value_,    uint16_t index_, uint8_t password_=0);
      void setF64Sys(double value_,   uint16_t index_, uint8_t password_=0);

      char *getStringUser(cUser_t *user_) const;
      inline char *getStringSys(void) const { return onGetString(); }

      void setStringUser(cUser_t *user_, char * value_,   uint8_t password_=0);
      void setStringSys(char *value_,  uint8_t password_=0);

    public:
      bool setPassword(uint8_t password_);
      bool clearPassword(uint8_t password_);

      inline eAppKey              getApp(void) const { return key.getApp(); }
      inline uint8_t                getInstance(void) const { return key.getInstance(); }
      inline bool                   getCfg(void) const { return key.getCfg(); }
      inline uint16_t               getDescription(void) const { return key.getDescription(); }
      inline eRegisterOrigin_t      getOrigin(void) const { return format.getOrigin(); }
      inline eRegisterPersistence_t getPersistence(void) const { return format.getPersistence(); }
      inline uint32_t               getKey(void) const { return key.getKey(); }

      inline bool allowRead(cUser_t *u) const { return getProfileRd().allow(u->getProfile()); }
      inline bool allowWrite(cUser_t *u) const { return getProfileWr().allow(u->getProfile()); }

  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTER_HPP_ */
