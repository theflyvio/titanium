#include <RegisterU08Ranged.hpp>

namespace Titanium::db {

  cRegisterU08Ranged_t::cRegisterU08Ranged_t(uint8_t ini_,
                                             uint8_t instance_,
                                             bool cfg_,
                                             uint16_t description_,
                                             eRegisterOrigin_t origin_, 
                                             eRegisterPersistence_t persistence_)
                       :cRegisterU08_t(ini_,
                                       instance_,
                                       cfg_,
                                       description_,
                                       origin_,
                                       persistence_){
  }

  cRegisterU08Ranged_t::~cRegisterU08Ranged_t(){
  }

  bool cRegisterU08Ranged_t::onSetU08(uint8_t value_, uint16_t index_) {
    const bool r = value_>=min && value_<=max;
    if(r){
      cRegisterU08_t::onSetU08(value_, 0);
    }
    return r;
  }

}
