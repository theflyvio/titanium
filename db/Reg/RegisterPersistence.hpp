#ifndef TITANIUM_DB_SFF_REG_TREGISTERPERSISTENCE_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERPERSISTENCE_HPP_

#include <stdint.h>
#include <etc.hpp>

namespace Titanium::db {

  enum class PACKED eRegisterPersistence_t : uint8_t {
    NEVER,
    ON_CHANGE,
    ON_SHUTDOWN
  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERPERSISTENCE_HPP_ */
