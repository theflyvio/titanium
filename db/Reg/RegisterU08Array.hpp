#ifndef TITANIUM_DB_REGISTERU08ARRAY_HPP_
#define TITANIUM_DB_REGISTERU08ARRAY_HPP_

#include <stdint.h>
#include <string.h>
#include <etc.hpp>
#include <Register.hpp>

namespace Titanium::db {

  template <uint16_t L>
  class PACKED cRegisterU08Array_t : public cRegister, public cValueArrayImpl_t<uint8_t, L> {
    protected:
      cRegisterU08Array_t(uint8_t ini_[],
                          uint16_t iniLength_,
                          uint8_t instance_,
                          bool cfg_,
                          uint16_t description_,
                          eRegisterOrigin_t origin_, 
                          eRegisterPersistence_t persistence_)
                :cRegister(instance_, 
                 cfg_,
                 description_,
                 eCommonType_t::U08, 
                 origin_, 
                 persistence_){
        this->setValueBulk(ini_, iniLength_);
      }

      uint8_t onGetU08(uint16_t index_) const override {
        return this->getValue(index_);
      }

      bool onSetU08(uint8_t value_, uint16_t index_) override {
        bool const changed = value_!=this->getValue(index_);
        this->setValue(value_, index_);
        return changed;
      }

    public:

      uint8_t get(uint16_t index_) const override { return getU08Sys(index_); }
      void set(uint8_t value_, uint16_t index_) override { setU08Sys(value_, index_); }

  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERU08_HPP_ */
