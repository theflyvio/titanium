#include <RegisterOne.hpp>

namespace Titanium::db {

  cRegisterOne_t::cRegisterOne_t(uint8_t ini_, 
                                 uint8_t instance_,
                                 bool cfg_,
                                 uint16_t description_,
                                 eRegisterOrigin_t origin_, 
                                 eRegisterPersistence_t persistence_)
    :cRegister(instance_, 
                 cfg_,
                 description_,
                 eCommonType_t::U08, 
                 origin_, 
                 persistence_){
    value = ini_;
  }

  uint8_t cRegisterOne_t::onGetU08(uint16_t index_) const {
    return value;
  }

  bool cRegisterOne_t::onSetU08(uint8_t value_, uint16_t index_) {
    bool const changed = value_!=value;
    value = value_;
    return changed;
  }

}
