#ifndef TITANIUM_DB_SFF_REG_TREGISTERORIGIN_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERORIGIN_HPP_

#include <stdint.h>
#include <etc.hpp>

namespace Titanium::db {

  enum class PACKED eRegisterOrigin_t : uint8_t {
    LOCAL,
    REMOTE
  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERORIGIN_HPP_ */
