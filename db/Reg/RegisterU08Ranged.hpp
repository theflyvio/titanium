#ifndef TITANIUM_DB_SFF_REG_TREGISTERU08RANGED_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERU08RANGED_HPP_

#include <etc.hpp>
#include <RegisterU08.hpp>

namespace Titanium::db {

  class PACKED cRegisterU08Ranged_t : public cRegisterU08_t {
    private:
      cUserProfile_t rd;
      cUserProfile_t wr;
      uint8_t min;
      uint8_t max;

    protected:
      inline void configure(cUserProfile_t rd_, cUserProfile_t wr_,uint8_t min_,uint8_t max_){
        rd = rd_;
        wr = wr_;
        min = min_;
        max = max_;
      }

      bool onSetU08(uint8_t value_, uint16_t index_) override;

      cUserProfile_t getProfileRd(void) const override { return rd; }
      cUserProfile_t getProfileWr(void) const override { return wr; }

    public:
      cRegisterU08Ranged_t(uint8_t ini_, 
                           uint8_t instance_,
                           bool cfg_,
                           uint16_t description_,
                           eRegisterOrigin_t origin_, 
                           eRegisterPersistence_t persistence_);
      ~cRegisterU08Ranged_t();

      friend cRegisterU08Ranged_t *cRegLocalCfgU08Ranged(uint8_t instance_,
                                                         uint16_t description_,
                                                         eRegisterPersistence_t persistence_,
                                                         uint8_t ini_,
                                                         cUserProfile_t rd_,
                                                         cUserProfile_t wr_,
                                                         uint8_t min_,
                                                         uint8_t max_);

      friend cRegisterU08Ranged_t *cRegRemoteCfgU08Ranged(uint8_t instance_,
                                                          uint16_t description_,
                                                          eRegisterPersistence_t persistence_,
                                                          uint8_t ini_);

      friend cRegisterU08Ranged_t *cRegLocalPrcU08Ranged(uint8_t instance_,
                                                         uint16_t description_,
                                                         eRegisterPersistence_t persistence_,
                                                         uint8_t ini_,
                                                         cUserProfile_t rd_,
                                                         cUserProfile_t wr_,
                                                         uint8_t min_,
                                                         uint8_t max_);

      friend cRegisterU08Ranged_t *cRegRemotePrcU08Ranged(uint8_t instance_,
                                                          uint16_t description_,
                                                          eRegisterPersistence_t persistence_,
                                                          uint8_t ini_);
};

  inline cRegisterU08Ranged_t *cRegLocalCfgU08Ranged(uint8_t instance_,
                                                     uint16_t description_,
                                                     eRegisterPersistence_t persistence_,
                                                     uint8_t ini_,
                                                     cUserProfile_t rd_,
                                                     cUserProfile_t wr_,
                                                     uint8_t min_,
                                                     uint8_t max_){
    cRegisterU08Ranged_t* r = new cRegisterU08Ranged_t(ini_,instance_,true,description_,eRegisterOrigin_t::LOCAL,persistence_);
    r->configure(rd_, wr_, min_, max_);
    return r;
  }

  inline cRegisterU08Ranged_t *cRegRemoteCfgU08Ranged(uint8_t instance_,
                                                      uint16_t description_,
                                                      eRegisterPersistence_t persistence_,
                                                      uint8_t ini_){
    return new cRegisterU08Ranged_t(ini_,instance_,true,description_,eRegisterOrigin_t::REMOTE,persistence_);
  }

  inline cRegisterU08Ranged_t *cRegLocalPrcU08Ranged(uint8_t instance_,
                                                     uint16_t description_,
                                                     eRegisterPersistence_t persistence_,
                                                     uint8_t ini_,
                                                     cUserProfile_t rd_,
                                                     cUserProfile_t wr_,
                                                     uint8_t min_,
                                                     uint8_t max_){
    cRegisterU08Ranged_t* r = new cRegisterU08Ranged_t(ini_,instance_,false,description_,eRegisterOrigin_t::LOCAL,persistence_);
    r->configure(rd_, wr_, min_, max_);
    return r;
  }

  inline cRegisterU08Ranged_t *cRegRemotePrcU08Ranged(uint8_t instance_,
                                                      uint16_t description_,
                                                      eRegisterPersistence_t persistence_,
                                                      uint8_t ini_){
    return new cRegisterU08Ranged_t(ini_,instance_,false,description_,eRegisterOrigin_t::REMOTE,persistence_);
  }

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERU08RANGED_HPP_ */
