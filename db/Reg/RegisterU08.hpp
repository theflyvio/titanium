#ifndef TITANIUM_DB_SFF_REG_TREGISTERU08_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERU08_HPP_

#include <stdint.h>
#include <etc.hpp>
#include <Register.hpp>

namespace Titanium::db {

  class PACKED cRegisterU08_t : public cRegister, public cValueImpl_t<uint8_t> {
    protected:
      cRegisterU08_t(uint8_t ini_,
                    uint8_t instance_,
                    bool cfg_,
                    uint16_t description_,
                    eRegisterOrigin_t origin_,
                    eRegisterPersistence_t persistence_);

      uint8_t onGetU08(uint16_t index_) const override;
      bool onSetU08(uint8_t value_, uint16_t index_) override;

    public:
      inline uint8_t get(void) const { return getU08Sys(0); }
      inline void set(uint8_t value_) { setU08Sys(value_, 0); }

  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERU08_HPP_ */
