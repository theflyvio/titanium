#include <RegisterU08.hpp>

namespace Titanium::db {

  cRegisterU08_t::cRegisterU08_t(uint8_t ini_, 
                                 uint8_t instance_,
                                 bool cfg_,
                                 uint16_t description_,
                                 eRegisterOrigin_t origin_, 
                                 eRegisterPersistence_t persistence_)
    :cRegister(instance_, 
                 cfg_,
                 description_,
                 eCommonType_t::U08, 
                 origin_, 
                 persistence_){
    setValue(ini_);
  }

  uint8_t cRegisterU08_t::onGetU08(uint16_t index_) const {
    return getValue();
  }

  bool cRegisterU08_t::onSetU08(uint8_t value_, uint16_t index_) {
    bool const changed = value_!=getValue();
    setValue(value_);
    return changed;
  }

}
