#ifndef TITANIUM_TDB_REGISTERLOCK_HPP_
#define TITANIUM_TDB_REGISTERLOCK_HPP_

#include <Register.hpp>
#include <stdint.h>

namespace Titanium::db {

  class cRegisterLock_t final {
    private:
      cRegister *reg;
      uint8_t password;

    public:
      cRegisterLock_t(cRegister *reg_);
      ~cRegisterLock_t();

      bool lock(void);
      void unlock(void);
      inline bool locked(void) { return password>0; }

  };

}

#endif
