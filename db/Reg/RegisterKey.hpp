#ifndef TITANIUM_DB_SFF_REG_TREGISTERKEY_HPP_
#define TITANIUM_DB_SFF_REG_TREGISTERKEY_HPP_

#include <appKey.hpp>
#include <stdint.h>
#include <etc.hpp>

namespace Titanium::db {
  
  class PACKED cRegisterKey_t final {
    private:
      union PACKED {
        struct PACKED {
          uint32_t app            : 6;
          uint32_t instance       : 6;
          uint32_t cfg            : 1; //boolean é config?
          uint32_t description    : 19;
        };
        uint32_t key;
      } composition;

    public:
      cRegisterKey_t(uint8_t instance,
                     bool cfg,
                     uint16_t description);

      void setApp(eAppKey app);
      inline eAppKey getApp(void) const { return eAppKey((uint8_t)composition.app); }
      inline uint8_t   getInstance(void) const { return composition.instance; }
      inline bool      getCfg(void) const { return composition.cfg; }
      inline uint8_t   getDescription(void) const { return composition.description; }

      inline uint32_t  getKey(void) const { return static_cast<uint32_t>(composition.key); }
  };

}

#endif /* TITANIUM_DB_SFF_REG_TREGISTERKEY_HPP_ */
