#include <RegisterFormat.hpp>
#include <stdint.h>

namespace Titanium::db {

  void cRegisterFormat_t::setOrigin(eRegisterOrigin_t origin){
    format.origin = origin;
  }

  eRegisterOrigin_t cRegisterFormat_t::getOrigin(void) const {
    return format.origin;
  }

  void cRegisterFormat_t::setPersistence(eRegisterPersistence_t persistence){
    format.persistence = persistence;
  }

  eRegisterPersistence_t cRegisterFormat_t::getPersistence(void) const {
    return format.persistence;
  }

  void cRegisterFormat_t::setCType(eCommonType_t ctype){
    format.ctype = ctype;
  }

  eCommonType_t cRegisterFormat_t::getCType(void) const {
    return format.ctype;
  }

}
