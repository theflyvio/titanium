#include <RegisterU08Generic.hpp>

namespace Titanium::db {

  cRegisterU08Generic_t::cRegisterU08Generic_t(uint8_t ini_,
                                              uint8_t instance_,
                                              bool cfg_,
                                              uint16_t description_,
                                              eRegisterOrigin_t origin_,
                                              eRegisterPersistence_t persistence_)
                        :cRegisterU08_t(ini_, instance_, cfg_, description_, origin_, persistence_){
  }


}
