/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : Titanium.hpp
 * @brief         : Main system modules.
 *                  Initialization and loop processes, operation system & database
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#ifndef __TITANIUM_HPP
#define __TITANIUM_HPP

#define TITANIUM_H_VER_MAJOR                                                                       1
#define TITANIUM_H_VER_MINOR                                                                       0

#include <Database.hpp>
#include <Task.hpp>
#include <appKey.hpp>
#include <Core.hpp>
#include <Board.hpp>

namespace Titanium {

  using namespace Titanium::db;
  using namespace Titanium::Tos;
  using namespace Titanium::Drivers;
  using namespace Titanium::Board;

  class cTitanium {
    public:
      enum eStep : uint8_t {
        eStartingApp,
        eCreatingBoard,
        eCreatingLocalRegisters,
        eCreatingRemoteRegisters,
        eCreatingTosTasks,
        eCreatingAppTasks,
        eCreatingSystem,
        eStartingRtos
      };

    private:
      static cTitanium* titanium;
      static uint32_t randomSeed;
      eStep appStep;
      eAppKey appKey;
      cBoard* board;

      void init(void);
      void loop(void);

    protected:
      cDatabase database;

      cTitanium(bool forceAsLeader_);
      //~cTitanium(); // singleton nunca destruido

      /**
       * instantiation of specific application board
       */
      virtual cBoard* createBoard(void) =0;
      virtual void createLocalRegisters(void) =0;
      virtual void createRemoteRegisters(void) =0;
      virtual void createSystem(void) =0;
      virtual void createAppTasks(void) =0;
      virtual void startRtosScheduler( void ) =0;

      eStep getAppStep(void) const;
      void setAppKey(uint8_t Value_);

      void add(cRegister* reg);
      void add(cTask* task);

    public:
      void begin(void);
      eAppKey getAppKey(void) const;
      cTitanium* getTitanium(void) const;
      static void setRandomSeed(uint32_t const seed);
      static uint32_t random(void);
      static uint32_t random(uint32_t const range);

      inline cBoard* getBoard(void) const;
  };

  inline cBoard* cTitanium::getBoard(void) const{
    return board;
  }

}

#endif /* __TITANIUM_H */
