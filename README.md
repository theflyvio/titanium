# Titanium Framework
## Introduction
This framework is intended to be used with Metroval products. The ```cTitanium``` class is derived into two other classes representing a leader and a follower application. All applications have a pointer to a board. The framework has a class designed to model electronic boards (class ```cBoard```) and each board contains one microcontroller represented by its model ```cCore```. For each core there are many peripherals like I2C, UART, timers, etc.

The application is responsible for deriving a board class from ```cBoard``` and a core class from ```cCore```.

## Titanium class (cTitanium)
Each Metroval product contains a microcontroller running an application. This class is responsible for all methods and members related to this type of application like:

* State
* Board pointer
* Application key
* Database
* Initialization and loop methods
* Setters and getters

Also, any derived class must implement the following pure virtual methods:

* createBoard
* createLocalRegisters
* createRemoteRegisters
* createSystem
* createAppTasks
* startRtosScheduler

To start the application, the system must call ```begin()``` method. It will initialize the framework and enter an infinite loop.

### Board class (cBoard)
Electronic boards contain components like temperature sensors, memories, RTC, leds, USB ports, ethernet ports, etc. These components are called ```devices```. Device classes are derived from ```cLock``` class. The ```cBoard``` derived class is responsible for creating and instantiating each device that is present in the physical board. One special device that must be present is a microcontroller represented by ```cCore``` class and will be explained later.

```
+-----------------------------------------------------------------------------+
|                             AxiomS [cBoard]                                 |
|                                                                             |
|                                                                             |
|        +---------------------------+                                        |
|        | STM32H753                 |                                        |
|        |  [cCore]                  |                                        |
|        |                           |                                        |
|        +-------+                   |                                        |
|        |  I2C1 |                   |                                        |
|        +-------+                   |                                        |
|        |   |                       |                                        |
|        +-------+-------+-------+---+                                        |
|        | GPIOA | GPIOB | GPIOK |...|                                        |
|        +-------+-------+-------+---+                                        |
|             |                                                               |
|             |                                                               |
|        +---------+                                                          |
|        |  CAT24  |                                                          |
|        | [cLock] |                                                          |
|        +---------+                                                          |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
+-----------------------------------------------------------------------------+
```

```cBoard``` is intended to be used as a singleton and is a pure virtual class. It has a private pointer to a ```cCore``` and its getter method. Also, there are methods for getting instances of each microcontroller peripheral like I2C or GPIO.

:warning: ==cBoard has a private method called init() that must be called before use==

This method calls cBoard's virtual methods ```newCore()```, responsible for instantiating a new core and ```createComponents()```. Also calls created core ```init()``` method.

```
cTitanium.begin() -> cTitanium.init() -> cTitanium.createBoard()# -> new application board
                                      -> cTitanium.board.init()  -> cTitanium.board.newCore()# -> new cCoreImpl()
                                                                 -> cTitanium.board.core.init()
                                                                 -> cTitanium.board.createComponents()#


```


## Core class

This class represents a microcontroller with its peripherals.

```cCore``` is intended to be used as a singleton and is a pure virtual class.

:warning: ==cCore has a private method called init() that must be called before use==

Any derived class must implement the following pure virtual methods: ```onInit``` and all methods that instantiate new peripherals like ``` newPort```, ```newGPIO``` and ```newI2C```.

## Árvore de Herança
cTitanium -> cTitaniumLeader -> cAxiomS

cThread -> cTask -> cMainTask
cThread -> cTask -> cBlinkLedTask

## Azure RTOS Initialization
### System Reset Vector & Entry point
All microprocessors have reset logic. When a reset occurs (either hardware or software), the address of the application's entry point is retrieved from a specific memory location. After the entry point is retrieved, the processor transfers control to that location. The application entry point is quite often written in the native assembly language and is usually supplied by the development tools (at least in template form). In some cases, a special version of the entry program is supplied with ThreadX.

Entry point is specified by the linker script file *.ld

```
/* Entry Point */
ENTRY(Reset_Handler)
```

Sample from startup file written in assembly:
```
Reset_Handler:
  ldr   sp, =_estack      /* set stack pointer */

/* Call the clock system initialization function.*/
  bl  SystemInit

...

/* Call the application's entry point.*/
  bl  main
```


### Development tool initialization
After the low-level initialization is complete, control transfers to the development tool's high-level initialization. This is usually the place where initialized global and static C variables are set up. Remember their initial values are retrieved from the constant area. Exact initialization processing is development tool specific.

### main()
When the development tool initialization is complete, control transfers to the user-supplied main function. At this point, the application controls what happens next. For most applications, the main function simply calls tx_kernel_enter, which is the entry into ThreadX. However, applications can perform preliminary processing (usually for hardware initialization) prior to entering ThreadX.
:warning: ==The call to tx_kernel_enter does not return, so do not place any processing after it.==

### tx_kernel_enter()
The entry function coordinates initialization of various internal ThreadX data structures and then calls the application's definition function **tx_application_define**.

When **tx_application_define** returns, control is transferred to the thread scheduling loop. This marks the end of initialization.

The sequence of calls of this application is as follows:

* function **main()**
* application **AxiomS.begin()** implemented in base class **cTitanium**
* **cTitanium.init()**
* **cAxiomS.startRtosScheduler()**
* **tx_kernel_enter()**
* **tx_application_define(mem_ptr)**
* enter thread scheduling loop

### tx_application_define(mem_pointer)
The **tx_application_define** function defines all of the initial application threads, queues, semaphores, mutexes, event flags, memory pools, and timers. It is also possible to create and delete system resources from threads during the normal operation of the application. However, all initial application resources are defined here.

The **tx_application_define** function has a single input parameter and it is certainly worth mentioning. The first-available RAM address is the sole input parameter to this function. It is typically used as a starting point for initial run-time memory allocations of thread stacks, queues, and memory pools.

:warning: ==After initialization is complete, only an executing thread can create and delete system resources— including other threads. Therefore, at least one thread must be created during initialization.==
