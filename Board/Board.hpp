#ifndef TITANIUM_DRIVER_BOARD_HPP_
#define TITANIUM_DRIVER_BOARD_HPP_

#include <stdint.h>
#include <Core.hpp>

namespace Titanium {
  class cTitanium;
}

namespace Titanium::Board {

  using namespace Titanium::Drivers;

  class cBoard {
    public:
      enum eStep : uint8_t {
        eNone, eInit,

      };

    private:
      static cBoard* board;
      cCore* core;
      eStep step;

      void init(void);

    protected:
      virtual cCore* newCore(void) = 0;
      virtual void createComponents(void) = 0;

    public:
      cBoard(void);  // singleton nunca destruido

      eStep getStep(void) const;
      cPin* getPin(ePortName p, uint8_t n) const;
      //
      cGPIO* newGPIO(cPin* const pin, cGPIOConfig* const config);
      cGPIO* newGPIO(ePortName portName, uint8_t pin, cGPIOConfig* const config);
      //
      cI2C* getI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_);
      //
      static cBoard& getBoard(void);
      cCore& getCore(void) const;

      friend class Titanium::cTitanium;
  };

  inline cBoard::eStep cBoard::getStep(void) const{
    return step;
  }

  inline cPin* cBoard::getPin(ePortName p, uint8_t n) const{
    return core->getPin(p, n);
  }

  inline cGPIO* cBoard::newGPIO(cPin* const pin, cGPIOConfig* const config){
    return core->newGPIO(pin, config);
  }

  inline cGPIO* cBoard::newGPIO(ePortName portName, uint8_t pin, cGPIOConfig* const config){
    return newGPIO(getPin(portName, pin), config);
  }

  inline cI2C* cBoard::getI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_){
    return core->getI2C(n_, speed_, sda_, scl_);
  }

  inline cBoard& cBoard::getBoard(void){
    return *board;
  }

  inline cCore& cBoard::getCore(void) const{
    return *core;
  }

}

#endif
