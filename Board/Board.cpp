#include "Board.hpp"

namespace Titanium::Board {

cBoard *cBoard::board = nullptr;

cBoard::cBoard(void) {
	step = eNone;
}

void cBoard::init(void) {
	step = eInit;
	if (board != nullptr) {
		throw 0;
	}
	board = this;
	//
	core = newCore();
	if (core == nullptr) {
		throw 0;
	}
	core->init();
	//
	createComponents();
	//
}

}
