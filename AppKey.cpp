#include <appKey.hpp>

namespace Titanium {

  void eAppKey::setValue(uint8_t value_){
    if(value!=(uint8_t)eAppKey::eFollowerXX){
      throw 0;  // Unchangeable appKey value
    }
    value = (eValue)value_;
  }
}
