/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : TitaniumLeader.hpp
 * @brief         : Provide ...
 *
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#ifndef __TITANIUM_LEADER_HPP
#define __TITANIUM_LEADER_HPP

#include <Titanium.hpp>

namespace Titanium {

  class cTitaniumLeader: public cTitanium {
    protected:
      cTitaniumLeader(void);
      //~cTitaniumLeader(); // singleton nunca destruido

      /**
       * @brief leader initialization, metroval modules
       *
       */
      void createSystem(void) override final;
  };
}

#endif /* __TITANIUM_LEADER_HPP */
