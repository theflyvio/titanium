/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : Titanium.cpp
 * @brief         : Program body.
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#include <Titanium.hpp>

namespace Titanium {

  cTitanium* cTitanium::titanium = nullptr;

  cTitanium::cTitanium(bool ForceAsLeader_)
      : appKey(ForceAsLeader_ ? eAppKey::eLeader : eAppKey::eFollowerXX){
    if(titanium==nullptr){
      titanium = this;
      appStep = eStep::eStartingApp;
    }else{
      throw 0;  // Only one instance of Titanium is allowed in application
    }
  }

  void cTitanium::init(void){
    /* Based board devices, performs the application board creation */
    appStep = eStep::eCreatingBoard;
    board = createBoard();
    board->init();

    /* Creates the application's local registers based on source code provided by user */
    appStep = eStep::eCreatingLocalRegisters;
    createLocalRegisters();

    /* Creates the application's remote registers based on source code provided by user */
    appStep = eStep::eCreatingRemoteRegisters;
    createRemoteRegisters();

    /* Creates internal tasks for the operational system */
    appStep = eStep::eCreatingTosTasks;
    // TODO Add a method to create OS tasks

    // inicializacao (registros remotos...) ocorre antes do inicio das tasks?
    appStep = eStep::eCreatingSystem;
    createSystem();

    /* Creates application tasks based on source code provided by user */
    /* Tasks must be created during ThreadX initialization or after */
    // appStep = eStep::eCreatingAppTasks;
    // createAppTasks();

    if( !appKey.IsInitialized()){
      throw 0;  // Initialization error
    }

    /* Usually this function doesn't return! */
    appStep = eStep::eStartingRtos;
    startRtosScheduler();
  }

  cTitanium::eStep cTitanium::getAppStep(void) const{
    return appStep;
  }

  void cTitanium::setAppKey(uint8_t Value_){
    appKey.setValue(Value_);
  }

  void cTitanium::loop(void){
    //vTaskStartScheduler();
  }

  void cTitanium::add(cRegister* reg){
    switch(appStep){
      case eStep::eCreatingLocalRegisters:
        if(reg->getOrigin()!=eRegisterOrigin_t::LOCAL){
          throw 0;  // Invalid register origin
        }
      break;

      case eStep::eCreatingRemoteRegisters:
        if(reg->getOrigin()!=eRegisterOrigin_t::REMOTE){
          throw 0;  // Invalid register origin
        }
      break;

      default: {
        throw 0;  // Invalid state
      }
    }
    database.add(reg);
  }

  void cTitanium::add(cTask* task){
    if(appStep!=eStep::eCreatingTosTasks&&appStep!=eStep::eCreatingAppTasks){
      throw 0;  // Invalid state
    }
    // add task (tasks podem ser verificadas/criadas/modificadas para diferencas entre titanium tasks e app tasks)
  }

  void cTitanium::begin(void){
    init();
    loop();
  }

  uint32_t cTitanium::random(void){
    //BSD
    //uint32_t r = (randomSeed = (1103515245*randomSeed+12345)/*&int.MaxValue* /);
    //MS
    //uint32_t r = (randomSeed = (214013*randomSeed+2531011)/*&int.MaxValue* /);
    //MTH$RANDOM
    return randomSeed = (69069*randomSeed+1)/*&int.MaxValue*/;
    // uint32_t next = randomSeed;
    // uint32_t result;
    // next *= 1103515245;
    // next += 12345;
    // result = (next / 65536) % 2048;
    // next *= 1103515245;
    // next += 12345;
    // result <<= 10;
    // result ^= (next / 65536) % 1024;
    // next *= 1103515245;
    // next += 12345;
    // result <<= 10;
    // result ^= (next / 65536) % 1024;
    // return randomSeed = next;
  }

  eAppKey cTitanium::getAppKey(void) const{
    return appKey;
  }

  cTitanium* cTitanium::getTitanium(void) const{
    return titanium;
  }

  void cTitanium::setRandomSeed(const uint32_t seed){
    randomSeed = seed;
  }

  uint32_t cTitanium::random(const uint32_t range){
    return random()%range;
  }
}
