#ifndef TITANIUM_OS_LOCK_HPP_
#define TITANIUM_OS_LOCK_HPP_

#include <etc.hpp>
#include <stdint.h>

namespace Titanium::os {

  class PACKED cLock {
    private:
      const void* locked;

      virtual uint8_t getLockDependencyCount(void) const;
      virtual cLock* getLockDependency(uint8_t const index) const;

    protected:
      virtual void onLock(void);
      virtual void onUnlock(void);

    public:
      cLock(void);

      void grantLocked(const void* const locker) const;
      bool isLocked(const void* const locker) const;
      bool lock(const void* const locker);
      bool unlock(const void* const locker);
  };

  inline cLock::cLock(void)
      : locked(nullptr){
  }

  inline uint8_t cLock::getLockDependencyCount(void) const{
    return 0;
  }

  inline cLock* cLock::getLockDependency(uint8_t const index) const{
    return nullptr;
  }

  inline void cLock::onLock(void){
  }

  inline void cLock::onUnlock(void){
  }

  inline bool cLock::isLocked(const void* const locker) const{
    return locked!=nullptr&&locked==locker;
  }

  inline void cLock::grantLocked(const void* const locker) const{
    if( !isLocked(locker)){
      throw 0;
    }
  }

}

#endif /* TITANIUM_OS_LOCK_HPP_ */
