#include "Lock.hpp"


namespace Titanium::os {

  /*
   * somente um exemplo, ainda tem que trocar por opcao threadsafe
   * provavelmente adicionar um mutex
   * verificar vantagens e se mantem controle de quem travou
   */
  bool cLock::lock(const void* const locker){
    bool allow = locked==nullptr && locker!=nullptr;
    if(allow){
      locked = locker;
      uint8_t dc = getLockDependencyCount();
      uint8_t i = 0;
      while(allow && i<dc){
        allow = getLockDependency(i)->lock(locker);
        if(allow){
          i++;
        }
      }
      if(allow){
        onLock();
      }else{
        unlock(locker);
      }
    }
    return allow;
  }

  bool cLock::unlock(const void* const locker){
    bool allow = locked!=nullptr && locked==locker;
    if(allow){
      uint8_t dc = getLockDependencyCount();
      for(uint8_t i=0; i<dc; i++){
        getLockDependency(i)->unlock(locker);
      }
      locked = nullptr;
      onUnlock();
    }
    return allow;
  }

}
