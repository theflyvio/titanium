/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : TitaniumApp.hpp
 * @brief         : Provide ...
 *
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#ifndef __APPKEY_HPP
#define __APPKEY_HPP

#include <stdint.h>

namespace Titanium {

  class eAppKey {
    public:
      enum eValue : uint8_t {
        eFollowerXX = 0, /* May be updated to : eFollower01 up to eFollower61 */
        eFollower01 = 1, /* Can't be changed */
        eFollower02 = 2, /* Can't be changed */
        eFollower03 = 3, /* Can't be changed */
        eFollower04 = 4, /* Can't be changed */
        eFollower05 = 5, /* Can't be changed */
        eFollower06 = 6, /* Can't be changed */
        eFollower07 = 7, /* Can't be changed */
        eFollower08 = 8, /* Can't be changed */
        eFollower09 = 9, /* Can't be changed */
        eFollower10 = 10, /* Can't be changed */
        eFollower11 = 11, /* Can't be changed */
        eFollower12 = 12, /* Can't be changed */
        eFollower13 = 13, /* Can't be changed */
        eFollower14 = 14, /* Can't be changed */
        eFollower15 = 15, /* Can't be changed */
        eFollower16 = 16, /* Can't be changed */
        eFollower17 = 17, /* Can't be changed */
        eFollower18 = 18, /* Can't be changed */
        eFollower19 = 19, /* Can't be changed */
        eFollower20 = 20, /* Can't be changed */
        eFollower21 = 21, /* Can't be changed */
        eFollower22 = 22, /* Can't be changed */
        eFollower23 = 23, /* Can't be changed */
        eFollower24 = 24, /* Can't be changed */
        eFollower25 = 25, /* Can't be changed */
        eFollower26 = 26, /* Can't be changed */
        eFollower27 = 27, /* Can't be changed */
        eFollower28 = 28, /* Can't be changed */
        eFollower29 = 29, /* Can't be changed */
        eFollower30 = 30, /* Can't be changed */
        eFollower31 = 31, /* Can't be changed */
        eFollower32 = 32, /* Can't be changed */
        eFollower33 = 33, /* Can't be changed */
        eFollower34 = 34, /* Can't be changed */
        eFollower35 = 35, /* Can't be changed */
        eFollower36 = 36, /* Can't be changed */
        eFollower37 = 37, /* Can't be changed */
        eFollower38 = 38, /* Can't be changed */
        eFollower39 = 39, /* Can't be changed */
        eFollower40 = 40, /* Can't be changed */
        eFollower41 = 41, /* Can't be changed */
        eFollower42 = 42, /* Can't be changed */
        eFollower43 = 43, /* Can't be changed */
        eFollower44 = 44, /* Can't be changed */
        eFollower45 = 45, /* Can't be changed */
        eFollower46 = 46, /* Can't be changed */
        eFollower47 = 47, /* Can't be changed */
        eFollower48 = 48, /* Can't be changed */
        eFollower49 = 49, /* Can't be changed */
        eFollower50 = 50, /* Can't be changed */
        eFollower51 = 51, /* Can't be changed */
        eFollower52 = 52, /* Can't be changed */
        eFollower53 = 53, /* Can't be changed */
        eFollower54 = 54, /* Can't be changed */
        eFollower55 = 55, /* Can't be changed */
        eFollower56 = 56, /* Can't be changed */
        eFollower57 = 57, /* Can't be changed */
        eFollower58 = 58, /* Can't be changed */
        eFollower59 = 59, /* Can't be changed */
        eFollower60 = 60, /* Can't be changed */
        eFollower61 = 61, /* Can't be changed */
        eUnknown = 62, /* Can't be changed */
        eLeader = 63, /* Can't be changed */
      };

    private:
      eValue value;

    public:

      eAppKey(const uint8_t value_);
      eAppKey(const eValue value_);

      bool isUnknown(void) const;
      bool IsLeader(void) const;
      bool IsFollower(void) const;
      bool IsInitialized(void) const;

      uint8_t getValue(void) const;

      void setValue(uint8_t value_);
  };

  inline eAppKey::eAppKey(const uint8_t value_){
    value = (eValue)value_;
  }

  inline eAppKey::eAppKey(const eValue value_){
    value = value_;
  }

  inline bool eAppKey::isUnknown(void) const{
    return (value==(uint8_t)eAppKey::eUnknown);
  }

  inline bool eAppKey::IsLeader(void) const{
    return (value==(uint8_t)eAppKey::eLeader);
  }

  inline bool eAppKey::IsFollower(void) const{
    return (value<=(uint8_t)eAppKey::eFollower61);
  }

  inline bool eAppKey::IsInitialized(void) const{
    return ((value!=(uint8_t)eAppKey::eFollowerXX)&&(value!=(uint8_t)eAppKey::eUnknown));
  }

  inline uint8_t eAppKey::getValue(void) const{
    return (uint8_t)value;
  }


}

#endif /* TITANIUM_TOS_TITANIUMAPP_HPP_ */
