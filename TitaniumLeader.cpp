/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : TitaniumLeader.cpp
 * @brief         : Program body.
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#include <TitaniumLeader.hpp>

namespace Titanium {
  cTitaniumLeader::cTitaniumLeader(void)
      : cTitanium(true){
  }

  void cTitaniumLeader::createSystem(void){
    /* Specific leader initialization */
  }
}
