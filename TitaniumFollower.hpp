/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : TitaniumFollower.hpp
 * @brief         : Provide ...
 *
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

#ifndef __TITANIUM_FOLLOWER_HPP
#define __TITANIUM_FOLLOWER_HPP

#include <Titanium.hpp>

namespace Titanium {
  class cTitaniumFollower: public cTitanium {
    protected:
      cTitaniumFollower(void);
      //~cTitaniumFollower(); aplicacao nunca é destruida

      /**
       * @brief follower initialization, metroval modules
       *
       */
      void createSystem(void) override final;
  };
}

#endif /* __TITANIUM_FOLLOWER_HPP */

