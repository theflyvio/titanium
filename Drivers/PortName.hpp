#ifndef DRIVERS_PORTNAME_HPP_
#define DRIVERS_PORTNAME_HPP_

#include <stdint.h>
#include <etc.hpp>

namespace Titanium::Drivers {

  class PACKED ePortName {
    public:
      enum eValue : uint8_t {
        A = 0, B, C, D, E, F, G, H, I, J, K
      };

      constexpr ePortName(const eValue value_);
      constexpr operator eValue(void) const;
      explicit operator bool(void) = delete;       // obriga if(mode==ePortName::A) / impede if(port)
      constexpr uint8_t getIndex(void) const;
      constexpr ePortName getNext(void) const;
      constexpr bool hasNext(void) const;
      static constexpr uint8_t getCount(void);
      static constexpr ePortName getFirst(void);
      static constexpr ePortName getLast(void);

    private:
      eValue value;
  };

  inline constexpr ePortName::ePortName(const eValue value_)
      : value(value_){
  }

  inline constexpr ePortName::operator eValue(void) const{
    return value;
  }

  inline constexpr uint8_t ePortName::getIndex(void) const{
    return (uint8_t)value;
  }

  inline constexpr ePortName ePortName::getNext(void) const{
    return value==K ? A : (eValue)(getIndex()+1);
  }

  inline constexpr bool ePortName::hasNext(void) const{
    return value!=getLast();
  }

  inline constexpr uint8_t ePortName::getCount(void){
    return getLast()+1;
  }

  inline constexpr ePortName ePortName::getFirst(void){
    return A;
  }

  inline constexpr ePortName ePortName::getLast(void){
    return K;
  }

}

#endif /* DRIVERS_PORTNAME_HPP_ */
