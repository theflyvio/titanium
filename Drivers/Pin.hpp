#ifndef TITANIUM_TOS_DRIVER_TPIN_HPP_
#define TITANIUM_TOS_DRIVER_TPIN_HPP_

#include <etc.hpp>
#include <Lock.hpp>
#include <stdint.h>

namespace Titanium::Drivers {

  using namespace Titanium::os;

  class PACKED ePinMode {
    public:
      enum eValue : uint8_t {
        pmINPUT, pmOUTPUT, pmALTERNATE, pmANALOG
      };

      constexpr ePinMode(const eValue value_);
      constexpr operator eValue(void) const;
      explicit operator bool(void) = delete;       // obriga if(mode==ePinMode::INPUT) / impede if(mode)

    private:
      const eValue value;

  };

  class PACKED ePinOutputType {
    public:
      enum eValue : uint8_t {
        PushPull, OpenDrain
      };

      constexpr ePinOutputType(const eValue value_);
      constexpr operator eValue(void) const;
      explicit operator bool(void) = delete;       // obriga if(mode==ePinMode::INPUT) / impede if(mode)

    private:
      const eValue value;

  };

  class PACKED ePinPull {
    public:
      enum eValue : uint8_t {
        NO_PULL, PULL_UP, PULL_DOWN
      };

      constexpr ePinPull(const eValue value_);
      constexpr operator eValue(void) const;
      explicit operator bool(void) = delete;       // obriga if(mode==ePinPull::PULL_UP) / impede if(mode)
      constexpr bool isNoPull(void) const;
      constexpr bool isPullUp(void) const;
      constexpr bool isPullDown(void) const;

    private:
      const eValue value;

  };

  class PACKED ePinSpeed {
    public:
      enum eValue : uint8_t {
        LOW, MEDIUM, HIGH, VERY_HIGH
      };

      constexpr ePinSpeed(const eValue value_);
      constexpr operator eValue(void) const;
      explicit operator bool(void) = delete;       // obriga if(mode==ePinSpeed::HIGH) / impede if(mode)

    private:
      const eValue value;

  };

  class PACKED ePinState {
    public:
      enum eValue : uint8_t {
        RESET = 0, SET
      };

      constexpr ePinState(const eValue value_);
      constexpr operator eValue(void) const;
      //explicit operator bool(void) = delete;       // obriga if(mode==ePinState::SET) / impede if(state)
      constexpr bool isReset(void) const;
      constexpr bool isSet(void) const;

    private:
      const eValue value;

  };

  class cPort;

  class PACKED cPin: public cLock {
    private:
      const cPort* port;
      const uint8_t pin;

    public:
      cPin(const cPort* port_, const uint8_t pin_);

      constexpr const cPort* getPort(void) const;
      constexpr uint8_t getN(void) const;
      constexpr uint16_t getPinMask(void) const;

      virtual void config(const ePinMode mode, const ePinOutputType outputType, const ePinPull pull,
          const ePinSpeed speed, uint32_t alternate) const =0;
      virtual uint8_t read(void) const =0;
      virtual void write(const void* const lock, const bool value) const =0;
      virtual void toggle(const void* const lock) const =0;
  };

  /*
   *
   */
  inline constexpr ePinMode::ePinMode(const eValue value_)
      : value(value_){
  }

  inline constexpr ePinMode::operator eValue(void) const{
    return value;
  }

  /*
   *
   */
  inline constexpr ePinOutputType::ePinOutputType(const eValue value_)
      : value(value_){
  }

  inline constexpr ePinOutputType::operator eValue(void) const{
    return value;
  }

  /*
   *
   */
  inline constexpr ePinPull::ePinPull(const eValue value_)
      : value(value_){
  }

  inline constexpr ePinPull::operator eValue(void) const{
    return value;
  }

  inline constexpr bool ePinPull::isNoPull(void) const{
    return value==NO_PULL;
  }

  inline constexpr bool ePinPull::isPullUp(void) const{
    return value==PULL_UP;
  }

  inline constexpr bool ePinPull::isPullDown(void) const{
    return value==PULL_DOWN;
  }

  /*
   *
   */
  inline constexpr ePinSpeed::ePinSpeed(const eValue value_)
      : value(value_){
  }

  inline constexpr ePinSpeed::operator eValue(void) const{
    return value;
  }

  /*
   *
   */
  inline constexpr ePinState::ePinState(const eValue value_)
      : value(value_){
  }

  inline constexpr ePinState::operator eValue(void) const{
    return value;
  }

  inline constexpr bool ePinState::isReset(void) const{
    return value==RESET;
  }

  inline constexpr bool ePinState::isSet(void) const{
    return value==SET;
  }

  /*
   *
   */
  inline cPin::cPin(const cPort* port_, const uint8_t pin_)
      : port(port_), pin(pin_){
  }

  inline constexpr const cPort* cPin::getPort(void) const{
    return port;
  }

  inline constexpr uint8_t cPin::getN(void) const{
    return pin;
  }

  inline constexpr uint16_t cPin::getPinMask(void) const{
    return 0x1<<pin;
  }

}

#endif /* TITANIUM_TOS_DRIVER_TPIN_HPP_ */
