#include <Core.hpp>

namespace Titanium::Drivers {

  cCore* cCore::core = nullptr;
  cI2C* cCore::first = nullptr;

  cCore::cCore(void){
  }

  void cCore::init(void){
    if(core!=nullptr){
      throw 0;
    }
    core = this;
    onInit();
    port = new cPort*[ePortName::getCount()];
    ePortName p(ePortName::A);
    do{
      port[p.getIndex()] = newPort(p);
      p = p.getNext();
    }while(p!=ePortName::A);
  }

  const cPort* cCore::getPort(const ePortName p) const{
    const cPort* const r = port[p.getIndex()];
    if(r==nullptr){
      throw 0;
    }
    return r;
  }

  cI2C* cCore::getI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_){
    cI2C* previous = nullptr;
    cI2C* i2c = first;
    while(i2c!=nullptr && !i2c->equals(n_, speed_, sda_, scl_)){
      previous = i2c;
      i2c = i2c->next;
    }
    if(i2c==nullptr){
      i2c = newI2C(n_, speed_, sda_, scl_);
      if(first==nullptr){
        first = i2c;
      }else{
        previous->next = i2c;
      }
    }
    return i2c;
  }

}
