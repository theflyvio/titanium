#include <GPIO.hpp>

#include <Core.hpp>

namespace Titanium::Drivers {

  cGPIOConfig cGPIOConfig::defaultInput(ePinMode::pmINPUT);
  cGPIOConfig cGPIOConfig::defaultOutput(ePinMode::pmOUTPUT);

  void cGPIO::onLock(void){
    // só muda configuracao se conseguiu lock
    pin->config(config->getMode(), config->getOutputType(), config->getPull(), config->getSpeed(), 0);
  }

}
