#ifndef TITANIUM_TOS_DRIVER_TGPIO_HPP_
#define TITANIUM_TOS_DRIVER_TGPIO_HPP_

#include <Lock.hpp>
#include <Pin.hpp>
#include <stdint.h>

namespace Titanium::Drivers {

  using namespace Titanium::os;

  /**
   *
   */
  class cGPIOConfig {
    private:
      const ePinMode mode;
      const ePinOutputType outputType;
      const ePinPull pull;
      const ePinSpeed speed;

    public:
      static cGPIOConfig defaultInput;
      static cGPIOConfig defaultOutput;

      cGPIOConfig(const ePinMode mode_, const ePinOutputType outputType_ = ePinOutputType::PushPull,
          const ePinPull pull_ = ePinPull::NO_PULL, const ePinSpeed speed_ = ePinSpeed::VERY_HIGH);

      ePinMode getMode(void) const;
      ePinOutputType getOutputType(void) const;
      ePinPull getPull(void) const;
      ePinSpeed getSpeed(void) const;
  };

  /**
   *
   */
  class cGPIO: public cLock {
    private:
      cPin* const pin;
      const cGPIOConfig* config;

      uint8_t getLockDependencyCount(void) const override;
      cLock* getLockDependency(uint8_t const index) const override;

      void Configure(void);

    protected:
      cGPIO(cPin* const pin_, const cGPIOConfig* const config_);
      void onLock(void) override;

    public:
      virtual ~cGPIO(){
      }
      ;
      void init(void);

      void setConfig(const cGPIOConfig* const config_);

      bool get(void) const;
      void set(const void* const locker) const;
      void reset(const void* const locker) const;
      void toggle(const void* const locker) const;

      bool read(void) const;
      void write(const void* const locker, const bool value) const;
  };

  /*
   *
   */
  inline cGPIOConfig::cGPIOConfig(ePinMode mode_, ePinOutputType outputType_, ePinPull pull_, ePinSpeed speed_)
      : mode(mode_), outputType(outputType_), pull(pull_), speed(speed_){
  }

  inline ePinMode cGPIOConfig::getMode(void) const{
    return mode;
  }

  inline ePinOutputType cGPIOConfig::getOutputType(void) const{
    return outputType;
  }

  inline ePinPull cGPIOConfig::getPull(void) const{
    return pull;
  }

  inline ePinSpeed cGPIOConfig::getSpeed(void) const{
    return speed;
  }

  /*
   *
   */
  inline cGPIO::cGPIO(cPin* const pin_, const cGPIOConfig* const config_)
      : pin(pin_), config(config_){
  }

  inline void cGPIO::setConfig(const cGPIOConfig* const config_){
    // NAO CONFIGURA AO SETAR, SÓ AO LOCKAR - isso garante que ninguem estraga ninguem
    config = config_;
  }

  inline uint8_t cGPIO::getLockDependencyCount(void) const{
    return 1;
  }

  inline cLock* cGPIO::getLockDependency(uint8_t const index) const{
    return pin;
  }

  inline bool cGPIO::get(void) const{
    return read();
  }

  inline void cGPIO::set(const void* const locker) const{
#warning verificar se o lock sera da instancia ou da thread
#warning o problema de ser da instancia
#warning é quando a mesma instancia é manipulada por multiplas threads. usar um semaphoro/mutex do OS
    pin->write(locker, true);
  }

  inline void cGPIO::reset(const void* const locker) const{
    pin->write(locker, false);
  }

  inline void cGPIO::toggle(const void* const locker) const{
    pin->toggle(locker);
  }

  inline bool cGPIO::read(void) const{
    return pin->read();
  }
  inline void cGPIO::write(const void* const locker, const bool value) const{
    pin->write(locker, value);
  }

}

#endif /* TITANIUM_TOS_DRIVER_TGPIO_HPP_ */
