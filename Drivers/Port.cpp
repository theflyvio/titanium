#include <Core.hpp>

namespace Titanium::Drivers {

  void cPort::init(const uint16_t validPins){
    for(uint8_t i = 0; i<16; i++){
      if(validPins&0x01){
        pin[i] = createPin(i);
        //pin[i]->deinit();
      }else{
        pin[i] = nullptr;
      }
    }
  }

  cPin* cPort::getPin(const uint8_t n) const{
    if(n>15||pin[n]==nullptr){
      throw 0;
    }
    return pin[n];
  }

}
