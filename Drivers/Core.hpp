#ifndef TITANIUM_DRIVER_CORE_HPP_
#define TITANIUM_DRIVER_CORE_HPP_

#include <Port.hpp>
#include <stdint.h>
#include <GPIO.hpp>
#include <I2C.hpp>

namespace Titanium::Board {
  class cBoard;
}

namespace Titanium::Drivers {

  class cCore {
    private:
      static cCore* core;
      static cI2C* first;
      //
      cPort** port;

      void init(void);
      virtual void onInit(void)=0;

      virtual cPort* newPort(const ePortName p) const =0;
      virtual cGPIO* newGPIO(cPin* const pin, const cGPIOConfig* const config) const =0;
      virtual cI2C* newI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_) const =0;

    public:
      cCore(void);  // singleton nunca destruido

      cPin* getPin(const ePortName p, const uint8_t n) const;
      cI2C* getI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_);
      const cPort* getPort(const ePortName p) const;
      static cCore& getCore(void);

      friend class Titanium::Board::cBoard;
      friend class cGPIO;
  };

  inline cPin* cCore::getPin(const ePortName p, const uint8_t n) const{
    return getPort(p)->getPin(n);
  }

  inline cCore& cCore::getCore(void){
    return *core;
  }

}

#endif
