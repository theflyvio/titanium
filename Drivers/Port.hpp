#ifndef TITANIUM_TOS_DRIVER_TPORT_HPP_
#define TITANIUM_TOS_DRIVER_TPORT_HPP_

#include <etc.hpp>
#include <Pin.hpp>
#include <PortName.hpp>
#include <stdint.h>
#include <Stream.hpp>

namespace Titanium::Drivers {

  class PACKED cPort {
    private:
      ePortName const portName;
      cPin* pin[16];

      virtual cPin* createPin(const uint8_t index) const =0;

    public:
      cPort(const ePortName portName_);
      void init(const uint16_t validPins);

      cPin* getPin(const uint8_t n) const;
      ePortName getPortName(void) const;

      friend class cCore;
  };

  inline cPort::cPort(const ePortName portName_)
      : portName(portName_){
  }

  inline ePortName cPort::getPortName(void) const{
    return portName;
  }

}

#endif /* TITANIUM_TOS_DRIVER_TPORT_HPP_ */
