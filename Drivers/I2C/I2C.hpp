#ifndef TITANIUM_DRIVER_TI2C_HPP_
#define TITANIUM_DRIVER_TI2C_HPP_

#include <Lock.hpp>
#include <Pin.hpp>

namespace Titanium::Drivers {

  using namespace Titanium::os;

  class eI2C {
    public:
      enum eValue : uint8_t {
        I2C_1 = 0, I2C_2 = 1, I2C_3 = 2, I2C_4 = 3
      };

      inline constexpr eI2C(eValue i2c)
          : value(i2c){
      }
      explicit operator bool() = delete; /* explicit is used to avoid implicit conversion and copy-initialization */
      inline operator eValue() const{
        return value;
      } /* user-defined conversion function */

    private:
      eValue value;

  };

  // class eI2CMode {
  //   public:
  //     enum eValue : uint8_t {
  //       MASTER = 0, SLAVE
  //     };

  //     inline constexpr eI2CMode(eValue mode)
  //         : value(mode){
  //     }
  //     explicit operator bool() = delete; /* explicit is used to avoid implicit conversion and copy-initialization */
  //     inline operator eValue() const{
  //       return value;
  //     } /* user-defined conversion function */

  //   private:
  //     eValue value;

  // };

  class eI2CSpeedMode {
    public:
      enum eValue : uint8_t {
        STANDARD, FAST, FAST_PLUS
      };

      inline constexpr eI2CSpeedMode(eValue speedMode)
          : value(speedMode){
      }
      explicit operator bool() = delete; /* explicit is used to avoid implicit conversion and copy-initialization */
      inline operator eValue() const{
        return value;
      } /* user-defined conversion function */

    private:
      eValue value;

  };
  
  class cI2C: public cLock {
    private:
      //
      static cLock i2cHw[4];
      static cI2C* configured[4];
      //
      const eI2C n;
      const eI2CSpeedMode speed;
      cI2C* next;

      virtual void config(void) const;

      uint8_t getLockDependencyCount(void) const override;
      cLock* getLockDependency(uint8_t const index) const override;

    protected:
      cPin* sda;
      cPin* scl;
      
      void onLock(void) override;
      eI2C getN(void) const;

    public:
      cI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_);

      virtual void writeMemory(uint16_t deviceAddress, uint16_t address, uint8_t* data, uint32_t len) const =0;
      virtual void readMemory(uint16_t deviceAddress, uint16_t address, uint8_t* data, uint32_t len) const =0;

      bool equals(const eI2C n_, const eI2CSpeedMode speed_, const cPin* sda_, const cPin* scl_) const;
      
      friend class cCore;
  };

  inline cI2C::cI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_)
      : n(n_), speed(speed_), sda(sda_), scl(scl_), next(nullptr){
  }

  inline void cI2C::config(void) const{
  }

  inline eI2C cI2C::getN(void) const{
    return n;
  }
}

#endif /* TITANIUM_DRIVER_TI2C_HPP_ */
