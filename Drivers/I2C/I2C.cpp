#include <I2C.hpp>

namespace Titanium::Drivers {

  cLock cI2C::i2cHw[4];
  cI2C* cI2C::configured[4] = {nullptr, nullptr, nullptr, nullptr};

  uint8_t cI2C::getLockDependencyCount(void) const {
    return 3;
  }

  cLock* cI2C::getLockDependency(uint8_t const index) const {
    switch(index){
      case 0:
        return i2cHw+n;

      case 1:
        return sda;

      default:
        return scl;
    }
  }

  void cI2C::onLock(void){
    // só muda configuracao se conseguiu lock
    if(configured[n]!=this){
      configured[n] = this;
      config();
    }
  }

  bool cI2C::equals(const eI2C n_, const eI2CSpeedMode speed_, const cPin* sda_, const cPin* scl_) const{
    return n==n_&&speed==speed_&&sda==sda_&&scl==scl_;
  }

}
